# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack
# :license: See README.rst

import argparse
import logging
import sys

import ftrack

import ftrack_connect_ceta.ceta as ceta
import ftrack_connect_ceta.sync as sync


def main(arguments=None):
    '''ftrack ceta connect.'''
    if arguments is None:
        arguments = []

    parser = argparse.ArgumentParser('ftrack_connect_ceta')

    # Allow setting of logging level from arguments.
    loggingLevels = {}
    for level in (
        logging.NOTSET, logging.DEBUG, logging.INFO, logging.WARNING,
        logging.ERROR, logging.CRITICAL
    ):
        loggingLevels[logging.getLevelName(level).lower()] = level

    parser.add_argument(
        'URL', type=str,
        help='URL of the Ceta Server to retrieve data from.'
    )

    parser.add_argument(
        '-p', '--port', type=int, default=80,
        help='Port of the Ceta Server to retrieve data from (defaults to 80).'
    )

    parser.add_argument(
        '-v', '--verbosity',
        help='Set the logging output verbosity.',
        choices=loggingLevels.keys(),
        default='info'
    )

    namespace = parser.parse_args(arguments)

    logging.basicConfig(level=loggingLevels[namespace.verbosity])

    cetaAPI = ceta.Ceta(namespace.URL, namespace.port)

    scheme = ftrack.getProjectSchemes().pop()
    project = sync.Project(
        cetaAPI=cetaAPI,
        defaultScheme=scheme
    )
    project.sync()

    planning = sync.Planning(
        cetaAPI=cetaAPI,
        acceptedCategories=('People',)
    )
    planning.sync()


if __name__ == '__main__':
    raise SystemExit(main(sys.argv[1:]))
