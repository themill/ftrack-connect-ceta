# :coding: utf-8
# :copyright: Copyright (c) 2014 ftrack
# :license: See README.rst

import urllib2
import json


class Ceta(object):
    '''Ceta REST API used for communication with a Ceta Server.'''

    def __init__(self, url, port=80):
        '''Initiate Ceta rest api with *url* and *port*.

        *port* is optional and defaults to 80.

        '''
        self.url = url
        self.port = port

    def __repr__(self):
        '''Return representation suitable for print and logging.'''
        return '<{0} on {1}:{2}>'.format(
            self.__class__.__name__, self.url, self.port
        )

    def _request(self, resource):
        '''Return response by requesting data from *resource*'''
        response = urllib2.urlopen('{0}/{1}'.format(self.url, resource))
        return json.loads(response.read())

    def getProjects(self):
        '''Retrieve projects from Ceta.'''
        return self._request('project')['data']
