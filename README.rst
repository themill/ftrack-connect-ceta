###################
ftrack Connect Ceta
###################

The ftrack integration with Ceta can be run as a standalone command or used as
basis for further development by importing classes from the sub-modules. 

Running it as a standalone command will connect to a Ceta server on the
specified URL and synchronize project data and bookings from it.

The synchronization is one-way and any changes in ftrack are not synced back to
Ceta. It consists of two separate parts:

1. Projects in Ceta are synced to ftrack with name, code, start and end dates. 
2. Resources and their schedules in Ceta are synced as Bookings assigned to 
users in ftrack. The Bookings are assigned based on the resources username. This
means that the username in Ceta must correspond to a username in ftrack. 

Re-running the command will synchronize again and update or add data where
appropriate. Items removed from a Resource's schedule in Ceta will result in the
corresponding booking being removed in ftrack. 

*****
Setup
*****

To run this example you need the ftrack API to be installed or present on your
PYTHONPATH.

The ftrack_connect_ceta module can be installed as:

.. code-block:: bash

    python setup.py install

The installed module can then run as a command: 

.. code-block:: bash
    
    python -m ftrack_connect_ceta <url-to-your-ceta-server>

*********************
Copyright and license
*********************

Copyright 2014 ftrack

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.